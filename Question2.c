#include<stdio.h>
int fibonacci(int);

int main(void){
    int t =11;
    
    for(int n = 0; n < t; n++){
        printf("\n%d", fibonacci(n));
        }
    return 0;
}

int fibonacci(int num){
    if(num == 0 || num == 1){
    return num;
    }
    else{
    return fibonacci(num-1) + fibonacci(num-2);
    }
}